package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.text.NumberFormat;
import java.util.Locale;

public class Calculator {


    public String evaluate(String statement) {

        LinkedList<Double> value = new LinkedList<Double>();
        LinkedList<Character> operators = new LinkedList<Character>();

        try {
            String mathExpression = statement.replaceAll(" ", "");

            for (int i = 0; i < mathExpression.length(); i++) {

                char ch = mathExpression.charAt(i);

                if (ch == '(') {
                    operators.add('(');

                } else if (ch == ')') {
                    while (operators.getLast() != '(') {
                        mathOperation(value, operators.removeLast());
                    }
                    operators.removeLast();

                } else if (ch == '+' || ch == '-' || ch == '*' || ch == '/') {
                    while (!operators.isEmpty() && priorityOperation(operators.getLast()) >= priorityOperation(ch)) {
                        mathOperation(value, operators.removeLast());
                    }
                    operators.add(ch);

                } else if (Character.isDigit(ch)) {
                    String operand = "";
                    while (i < mathExpression.length() && (mathExpression.charAt(i) == '.' || Character.isDigit(mathExpression.charAt(i)) || mathExpression.charAt(i) == ',')) {
                        if (mathExpression.charAt(i) == ',') {
                            return null;
                        }
                        operand += mathExpression.charAt(i++);
                    }
                    --i;
                    try {
                        value.add(Double.parseDouble(operand));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                }
            }

            while (!operators.isEmpty()) {
                mathOperation(value, operators.removeLast());
            }

        } catch (Exception e) {
            return null;
        }

        try {
            NumberFormat numberFormatter = NumberFormat.getNumberInstance(Locale.US);
            numberFormatter.setMaximumFractionDigits(4);
            numberFormatter.setMinimumFractionDigits(0);
            return numberFormatter.format(value.get(0));
        } catch (Exception e) {
            return null;
        }
    }


    public int priorityOperation(char operator) {
        if (operator == '*' || operator == '/') {
            return 1;
        } else if (operator == '+' || operator == '-') {
            return 0;
        } else {
            return -1;
        }
    }

    public void mathOperation(LinkedList<Double> st, char operator) throws ArithmeticException {

        double one = st.removeLast();
        double two = st.removeLast();

        switch (operator) {
            case '+':
                st.add(two + one);
                break;
            case '-':
                st.add(two - one);
                break;
            case '*':
                st.add(two * one);
                break;
            case '/':
                if (one == 0) {
                    throw new ArithmeticException();
                }
                st.add(two / one);
                break;
            default:
                System.out.println("Wrong operator value");
        }
    }
}
