package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {
        
    private static int checkPyramid(List<Integer> input) {
        if (input.isEmpty()) {
            throw new CannotBuildPyramidException();
        }
        int lenList = input.size();
        int d = 1 + 8 * lenList;
        if (d >= 0) {
            double x1, x2;
            x1 = (-1 + Math.sqrt(d)) / 2;
            x2 = (-1 - Math.sqrt(d)) / 2;

            if (x1 == (int) x1) {
                return (int) x1;
            } else if (x2 == (int) x2) {
                return (int) x2;
            }
        }
        return -1;
    }

    public static int[][] buildPyramid(List<Integer> inputNumbers){
        if (checkPyramid(inputNumbers) == -1) throw new CannotBuildPyramidException();
        int rows = checkPyramid(inputNumbers);
        int cols = 2 * rows - 1;
        try {
            Collections.sort(inputNumbers);
        }
        catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }
        int middle = (cols - 1) / 2;
        int[][] outPyramid = new int[rows][cols];
        int k = 0;
        for (int i = 0; i < rows; i++){
            for (int j = middle - i; j <= middle + i; j += 2) {
                outPyramid[i][j] = inputNumbers.get(k);
                k++;
            }
        }
        return outPyramid;
    }
}
