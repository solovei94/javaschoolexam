package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.size() == 0) {
            return true;
        }

        if (y.containsAll(x)) {
            for (Object i : x) {
                int elementFromX = y.indexOf(i);
                if (elementFromX == -1) return false;
                else {
                    y = y.subList(elementFromX + 1, y.size());
                }
            }
            return true;
        }      
        return false;
    }
}
